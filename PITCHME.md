# Algoritmos e Linguagens de Programação

Aula 01 - Apresentação

Prof. Luiz Carlos Melo Muniz

lcmuniz@gmail.com

---

### O Professor

- Luiz Carlos Melo Muniz
- Doutorando em Ciência da Computação (UFMA)
- Mestre em Ciência da Computação (UFMA)
- Especialista em Análise de Sistemas (UFMA)

- Email: lcmuniz@gmail.com
- Telefone: 9 9618 7238

---

### A Disciplina

- Disciplina de introdução à programação
- Solucionar um problema dividindo-o em vários passos
- Implementar uma solução em uma linguagem de programação

---

### Um Algoritmo

@ol

- Retirar o telefone do gancho
- Esperar o sinal
- Colocar o cartão
- Discar o número
- Falar no telefone
- Colocar o telefone no gancho

@olend

---

### Um Algoritmo Computacional

```
algoritmo "somar dois números"
  solicitar primeiro número
  solicitar segundo número
  somar os dois números
  mostrar o resultado 
```
---

### Uma Línguagem de Programação (Python)

```python
def somar_dois_numeros:
  x = input('Digite o primeiro número:')
  y = input('Digite o segundo número:')
  r = x + y
  print('O resultado é', r) 
```
---

### A Metodologia

- Aulas teóricas e práticas
- Google Classroom
- Todo o material apresentado estará disponível *online*
- O aluno pode contactar o professor por email, Whatsapp ou Google Chat

---

### Horas e Assiduidade

- Setenta e duas horas
- Trinta e seis aulas
- Máximo de nove faltas

---

### Avaliação

- Exercícios na plataforma Google Classroom
- Duas avaliações em sala de aula
- Um trabalho em equipe a ser apresentado no final do semestre

---

### Bibliografia

- **Algoritmos e Lógica de Programação**. Marco Antonio Furlan de Souza. Editora Cengage.
- **Algoritmos. Técnicas de Programação**. José Augusto N. G. Manzano,‎ Ecivaldo Matos e‎ André Evandro Lourenço. Editora Érica.
- **Introdução a Algoritmos e Programação com Python**. Raul Wazlawick. Editora Elsevier.
- **Lógica de Programação. A Construção de Algoritmos e Estruturas de Dados**. Andre Luis Forbellone e‎ Henri Eberspacher. Editora Pearson.
